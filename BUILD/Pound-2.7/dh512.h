#ifndef HEADER_DH_H
#include <openssl/dh.h>
#endif
DH *get_dh512()
	{
	static unsigned char dh512_p[]={
		0xA8,0xCB,0x6B,0xAE,0xCB,0xB8,0x5E,0x69,0x40,0xAA,0x38,0x9D,
		0x61,0x70,0x10,0x78,0x3E,0xFD,0x8C,0x8A,0x27,0x72,0x32,0x74,
		0xCB,0x10,0x49,0xBB,0x8B,0x0D,0xCA,0xA3,0x8C,0xE9,0xBC,0xEC,
		0x2B,0xB4,0x38,0xF7,0x91,0xFE,0xED,0x92,0x86,0xDB,0x3B,0x4E,
		0x37,0x36,0xA8,0x15,0xBA,0xA1,0xE4,0x8A,0x3F,0xAC,0xCC,0xA9,
		0xDC,0x16,0x17,0x33,
		};
	static unsigned char dh512_g[]={
		0x05,
		};
	DH *dh;

	if ((dh=DH_new()) == NULL) return(NULL);
	dh->p=BN_bin2bn(dh512_p,sizeof(dh512_p),NULL);
	dh->g=BN_bin2bn(dh512_g,sizeof(dh512_g),NULL);
	if ((dh->p == NULL) || (dh->g == NULL))
		{ DH_free(dh); return(NULL); }
	return(dh);
	}
